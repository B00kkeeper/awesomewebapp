﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AwesomeWebApp.DAL
{
    public interface IApplicationDbContext
    {
        IMongoCollection<Publisher> Publishers { get; }
    }
}
