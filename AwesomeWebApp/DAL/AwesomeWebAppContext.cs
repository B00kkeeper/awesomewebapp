﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace AwesomeWebApp.DAL
{
    public class AwesomeWebAppContext : DbContext
    {
        public AwesomeWebAppContext (DbContextOptions<AwesomeWebAppContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Book>(entity =>
            {
                entity.HasKey(e => e.BookId);
                entity.Property(e => e.Name).IsRequired();
            });
            
            //modelBuilder.Entity<Book>(entity =>
            //{
            //    entity.HasKey(e => e.ISBN);
            //    entity.Property(e => e.Title).IsRequired();
            //    entity.HasOne(d => d.Publisher)
            //      .WithMany(p => p.Books);
            //});

            modelBuilder.Entity<Book>().HasData(
              new Book { BookId = 1, Name = "MSDN Book" },
              new Book { BookId = 2, Name = "Docker Book" },
              new Book { BookId = 3, Name = "EFCore Book" }
            );
        }

        public DbSet<Book> Books { get; set; }
    }
}
