﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AwesomeWebApp.DAL
{
    public class Book
    {
        public int BookId { get; set; }
        public string Name { get; set; }
    }
}
