﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AwesomeWebApp.DAL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MongoDB.Bson;

namespace AwesomeWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PublishersController : ControllerBase
    {
        private readonly IPublishersRepository _repository;

        public PublishersController(IPublishersRepository repository)
        {
            _repository = repository;
        }

        // GET: api/Publishers
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Publisher>>> Get()
        {
            return new JsonResult(await _repository.GetAll());
        }

        // GET: api/Publishers/5
        [HttpGet("{name}", Name = "Get")]
        public async Task<ActionResult<Publisher>> Get(string name)
        {
            return await _repository.Get(name);
        }

        // POST: api/Publishers
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Publisher publisher)
        {
            if (!string.IsNullOrEmpty(publisher.Id))
                return BadRequest("Object Id is not empty");

            await _repository.Create(publisher);

            return Ok();
        }

        // PUT: api/Publishers/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put([FromRoute]string id, [FromBody] Publisher publisher)
        {
            if (id != publisher.Id)
            {
                return BadRequest();
            }

            await _repository.Update(publisher);

            return Ok();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            await _repository.Delete(id);

            return Ok();
        }
    }
}
