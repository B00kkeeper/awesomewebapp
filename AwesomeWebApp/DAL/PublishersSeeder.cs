﻿using Bogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AwesomeWebApp.DAL
{
    public static class PublishersSeeder
    {
        public static void SeedData(IPublishersRepository repository)
        {
            var faker = new Faker<Publisher>()                
                .RuleFor(r => r.Country, "United States")
                .RuleFor(r => r.Name, f => $"{f.Company.CatchPhrase()} publisher");

            var publishers = faker.Generate(12);
            publishers.ForEach(pub => repository.Create(pub));
        }
    }
}
