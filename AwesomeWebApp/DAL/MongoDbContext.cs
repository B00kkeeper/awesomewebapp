﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AwesomeWebApp.DAL
{
    public class MongoDbContext : IApplicationDbContext
    {
        private readonly IMongoDatabase _db;
        public MongoDbContext(IOptions<Settings> options)
        {
            var client = new MongoClient(options.Value.MongoConnectionString);
            _db = client.GetDatabase(options.Value.MongoDB);
        }
        public IMongoCollection<Publisher> Publishers => _db.GetCollection<Publisher>("Publishers");
    }
}
