﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using AwesomeWebApp.DAL;

namespace AwesomeWebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddDbContext<AwesomeWebAppContext>(options =>
                options.UseMySQL(Configuration.GetConnectionString("AwesomeWebAppContext")));

            services.Configure<Settings>(
              settings =>
              {
                  settings.MongoDB = Configuration.GetSection("MongoDb:Database").Value;
                  settings.MongoConnectionString = Configuration.GetSection("MongoDb:MongoConnectionString").Value;
              });


            services.AddTransient<IApplicationDbContext, MongoDbContext>();
            services.AddTransient<IPublishersRepository, PublishersRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
