﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AwesomeWebApp.DAL
{
    public interface IPublishersRepository
    {
        Task<IEnumerable<Publisher>> GetAll();
        Task<Publisher> Get(string name);
        Task Create(Publisher publisher);
        Task<bool> Update(Publisher publisher);
        Task<bool> Delete(string id);
    }
}
