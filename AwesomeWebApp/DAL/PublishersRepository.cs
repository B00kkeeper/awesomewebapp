﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AwesomeWebApp.DAL
{
    public class PublishersRepository : IPublishersRepository
    {
        private readonly IApplicationDbContext _context;

        public PublishersRepository(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Publisher>> GetAll()
        {
            return await _context.Publishers.Find(_ => true).ToListAsync();
        }

        public async Task<Publisher> Get(string name)
        {
            var filter = Builders<Publisher>.Filter.Eq(m => m.Name, name);

            return await _context.Publishers.Find(filter).FirstOrDefaultAsync();
        }

        public async Task Create(Publisher publisher)
        {
            await _context.Publishers.InsertOneAsync(publisher);
        }


        public async Task<bool> Update(Publisher publisher)
        {
            var updateResult =
              await _context
              .Publishers
              .ReplaceOneAsync(filter: g => g.Id == publisher.Id, replacement: publisher);

            return updateResult.IsAcknowledged && updateResult.ModifiedCount > 0;
        }


        public async Task<bool> Delete(string id)
        {
            var filter = Builders<Publisher>.Filter.Eq(m => m.Id, id);

            var deleteResult = await _context
            .Publishers
            .DeleteOneAsync(filter);

            return deleteResult.IsAcknowledged && deleteResult.DeletedCount > 0;
        }
    }
}
