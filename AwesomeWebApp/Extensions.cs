﻿using AwesomeWebApp.DAL;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AwesomeWebApp
{
    public static class Extensions
    {
        public static IHost UpdateDatabase<T>(this IHost webHost) where T : DbContext
        {
            using (var scope = webHost.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                try
                {
                    var db = services.GetRequiredService<T>();

                    db.Database.EnsureCreated();

                    db.Database.ExecuteSqlCommand("CREATE TABLE IF NOT EXISTS `__EFMigrationsHistory` ( `MigrationId` nvarchar(150) NOT NULL, `ProductVersion` nvarchar(32) NOT NULL, PRIMARY KEY (`MigrationId`) );");

                    db.Database.Migrate();

                    // Seed MongoDb
                    var mongoDb = scope.ServiceProvider.GetService<IPublishersRepository>();

                    if (mongoDb.GetAll().Result.Count() == 0)
                    {
                        PublishersSeeder.SeedData(mongoDb);
                    }
                }
                catch (Exception ex)
                {
                    var logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "An error occurred while updating the database.");
                }
            }
            return webHost;
        }
    }
}
