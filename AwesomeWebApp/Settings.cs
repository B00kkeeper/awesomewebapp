﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AwesomeWebApp
{
    public class Settings
    {
        public string MongoDB { get; set; }
        public string MongoConnectionString { get; set; }
    }
}
